package com.vm.demoqa;

import com.vm.demoqa.driver.DriverSingleton;
import com.vm.demoqa.pages.HomePage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest {
  protected HomePage homePage;

  @BeforeClass
  public void beforeTest() {
    homePage=new HomePage().openPage();
  }

  @AfterClass
  public void closePage() {
    DriverSingleton.closeDriver();
  }
}