package com.vm.demoqa;

import com.vm.demoqa.client.ListOfBooksClient;
import com.vm.demoqa.pages.LoginPage;
import com.vm.demoqa.pojo.Book;
import com.vm.demoqa.utils.ConfigEnum;
import com.vm.demoqa.utils.ConfigReader;
import com.vm.demoqa.utils.DataBooksExtractUtils;
import io.restassured.response.Response;
import java.util.ArrayList;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;

public class HomePageTest extends BaseTest {

  LoginPage loginPage = new LoginPage();

  @Test
  public void testForCheckingListOfBooksBetweenApiAndUiParts() {
    homePage.clickButtonLoginOnHomePage();
    loginPage.inputUserName(ConfigReader.getValue(ConfigEnum.NAME))
             .inputPassword(ConfigReader.getValue(ConfigEnum.PASSWORD))
             .pressButtonLoginOnLoginPage();

    List<String> resultFromApiRequest = new ArrayList<>();
    Response listOfBooksClient = new ListOfBooksClient().getListOfBooks(ConfigReader.getValue(
        ConfigEnum.URL_API));
    List<Book> listFromApiRequest = DataBooksExtractUtils.getBooksList(
        listOfBooksClient.getBody().asString());
    for (Book book : listFromApiRequest) {
      resultFromApiRequest.add(book.getTitle());
    }
    Assert.assertEquals(homePage.getListOfBooks(), resultFromApiRequest);
  }
}
