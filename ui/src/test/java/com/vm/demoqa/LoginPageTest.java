package com.vm.demoqa;

import static org.hamcrest.MatcherAssert.assertThat;

import com.vm.demoqa.data.UsersData;
import com.vm.demoqa.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginPageTest extends BaseTest {

  LoginPage loginPage = new LoginPage();
  String expectedResult = "New User";

  @Test(dataProvider = "users data", dataProviderClass = UsersData.class)
  public void testByCreateUser(String name, String password) {
    homePage.clickButtonLoginOnHomePage();
    loginPage.inputUserName(name).inputPassword(password).pressButtonLoginOnLoginPage()
        .clickButtonLogOut();
    Assert.assertEquals(loginPage.getTextButtonNewUser().toString(), expectedResult);
  }
}
