package com.vm.demoqa.pages;

import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Getter
public class LoginPage extends AbstractPage {

  @FindBy(id = "userName")
  private WebElement fieldUserName;
  @FindBy(id = "password")
  private WebElement fieldPassword;
  @FindBy(id = "login")
  private WebElement buttonLoginOnLoginPage;
  @FindBy(id = "submit")
  private WebElement buttonLogOut;
  @FindBy(id = "newUser")
  private WebElement buttonNewUser;

  public LoginPage inputUserName(String name) {
    fieldUserName.sendKeys(name);
    return this;
  }

  public LoginPage inputPassword(String password) {
    fieldPassword.sendKeys(password);
    return this;
  }

  public LoginPage pressButtonLoginOnLoginPage() {
    buttonLoginOnLoginPage.click();
    return this;
  }

  public LoginPage clickButtonLogOut() {
    buttonLogOut.click();
    return this;
  }

  public String getTextButtonNewUser() {
    return buttonNewUser.getText();
  }
}
