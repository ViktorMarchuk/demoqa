package com.vm.demoqa.pages;

import com.vm.demoqa.utils.ConfigEnum;
import com.vm.demoqa.utils.ConfigReader;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Getter
public class HomePage extends AbstractPage {

  private String xpathListOfBooks = "//div[@class='action-buttons']";
  @FindBy(id = "login")
  private WebElement buttonLoginOnHomePage;

  public HomePage openPage() {
    driver.navigate().to(ConfigReader.getValue(ConfigEnum.BASE_URL));
    return this;
  }

  public HomePage clickButtonLoginOnHomePage() {
    buttonLoginOnHomePage.click();
    return this;
  }

  public List<String> getListOfBooks() {
    List<String> resultList = new ArrayList<>();
    List<WebElement> elementList = driver.findElements(By.xpath(xpathListOfBooks));
    for (WebElement element : elementList) {
      resultList.add(element.getText());
    }
    return resultList;
  }
}
