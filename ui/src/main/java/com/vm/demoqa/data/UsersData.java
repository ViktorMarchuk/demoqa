package com.vm.demoqa.data;

import org.testng.annotations.DataProvider;

public class UsersData {

  @DataProvider(name = "users data")
  public static Object[][] provideUsersData() {
    return new Object[][]{{"Test", "rt76HAhh46$"},
        {"Dan", "ot&87HG$#"}};
  }
}
