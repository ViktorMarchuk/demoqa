package com.vm.demoqa;

import com.vm.demoqa.pages.AbstractPage;
import com.vm.demoqa.utils.ConfigEnum;
import com.vm.demoqa.utils.ConfigReader;

public class HomePage extends AbstractPage {
  public HomePage openPage(){
    driver.navigate().to(ConfigReader.getValue(ConfigEnum.BASE_URL));
    return this;
  }
}
