package com.vm.demoqa.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.ToString;
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class Book {
    private String title;
    private String author;
    private String publisher;
    private int pages;
    private String description;
    private String website;
  }
