package com.vm.demoqa.specification;

import com.vm.demoqa.utils.ConfigEnum;
import com.vm.demoqa.utils.ConfigReader;
import io.restassured.RestAssured;

public class RestClient {

  public RestClient() {
    RestAssured.baseURI = ConfigReader.getValue(ConfigEnum.URL_API);
  }

  public  void closeClient() {
    RestAssured.reset();
  }
}
