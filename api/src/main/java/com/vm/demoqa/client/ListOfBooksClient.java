package com.vm.demoqa.client;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class ListOfBooksClient {

  public Response getListOfBooks(String api_url) {
    return RestAssured
        .given()
        .when()
        .get(api_url);
  }
  public Response postUser(String api_url){
    return RestAssured
        .given()
        .when()
        .post(api_url);
  }
}
