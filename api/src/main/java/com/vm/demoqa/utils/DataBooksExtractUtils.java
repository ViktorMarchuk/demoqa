package com.vm.demoqa.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vm.demoqa.pojo.Book;
import com.vm.demoqa.pojo.BookCatalog;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class DataBooksExtractUtils {

  public static List<Book> getBooksList(String json) {
    ObjectMapper mapper = new ObjectMapper();
    List<Book> searchResult = new ArrayList<>();
    try {
      BookCatalog bookCatalog = mapper.readValue(json, BookCatalog.class);
      searchResult = bookCatalog.getBooks();
    } catch (JsonProcessingException e) {
      log.error("Error while processing JSON: " + e.getMessage());
    }
    log.info(String.format("List of books: %s", searchResult));
    return searchResult;
  }
}