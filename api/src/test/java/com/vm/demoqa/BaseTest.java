package com.vm.demoqa;

import com.vm.demoqa.specification.RestClient;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {

  protected static RestClient restClient;

  @BeforeTest
  public static void setUp() {
    restClient = new RestClient();
  }

  @AfterTest
  public static void tearDawn() {
    restClient.closeClient();
  }
}
