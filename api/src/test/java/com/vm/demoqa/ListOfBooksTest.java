package com.vm.demoqa;

import com.vm.demoqa.client.ListOfBooksClient;
import com.vm.demoqa.pojo.Book;
import com.vm.demoqa.utils.ConfigEnum;
import com.vm.demoqa.utils.ConfigReader;
import com.vm.demoqa.utils.DataBooksExtractUtils;
import io.restassured.response.Response;
import java.util.ArrayList;
import java.util.List;
import org.testng.annotations.Test;

public class ListOfBooksTest extends BaseTest {

  @Test
  public void testList() {
    Response listOfBooksClient = new ListOfBooksClient().getListOfBooks(
        ConfigReader.getValue(ConfigEnum.URL_API));
   List<String> result=new ArrayList<>();
    List<Book> listOfBooks = DataBooksExtractUtils.getBooksList(
        listOfBooksClient.getBody().asString());
    for (Book book : listOfBooks) {
      result.add(String.valueOf(book.getPages()));
    }
      System.out.println(result);
    }
  }

