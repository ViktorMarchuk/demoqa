package com.vm.demoqa.utils;

import java.util.ResourceBundle;

public class ConfigReader {
  private static final  ResourceBundle RB = ResourceBundle.getBundle("config");

  public static String getValue(ConfigEnum value){
    return RB.getString(value.getParam());
  }
}
