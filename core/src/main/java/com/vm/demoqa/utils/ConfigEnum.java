package com.vm.demoqa.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter

public enum ConfigEnum {
  BASE_URL("baseUrl"),
  NAME("userName"),
  PASSWORD("password"),
  URL_API("urlApi");
  private  String param;
}
