package com.vm.demoqa.pages;

import com.vm.demoqa.driver.DriverSingleton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class AbstractPage {

  protected WebDriver driver;

  public AbstractPage() {
    this.driver = DriverSingleton.getDriver();
    PageFactory.initElements(driver, this);
  }
}